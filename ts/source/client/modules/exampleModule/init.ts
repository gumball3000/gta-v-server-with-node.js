import alt, { PointBlip } from 'alt'
import native from 'natives'
import exampleFunction from 'client/modules/exampleModule/functions/exampleFunction'

export default async () => {
	await exampleFunction()
	alt.log('exampleModule inited')


}

if (!native.hasAnimDictLoaded('mp_suicide')) {
	native.requestAnimDict('mp_suicide');
}

alt.on("update",()=>{
	var waypoint:any = native.getFirstBlipInfoId(8);
	

	if (native.doesBlipExist(waypoint)) {
		let coords: Vector3 = native.getBlipInfoIdCoord(waypoint);
		//alt.Player.local.pos = coords;
		//alt.getLocalPlayer().scriptID().pos = coords;

		var res:any = native.getGroundZFor3dCoord(coords.x, coords.y, coords.z + 100, undefined, undefined);

		coords.z = res + 1;
		native.clearGpsPlayerWaypoint();
		alt.emitServer('tpToBlip', coords);
	}

	// blips.forEach((blip:alt.PointBlip,player:alt.Player)=>{
	// 	let positionArray: number[] = [];
	// 	positionArray.push(player.pos.x);
	// 	positionArray.push(player.pos.y);
	// 	positionArray.push(player.pos.z);
	// 	blip.position = positionArray;
	
	// });
});

alt.onServer("giveWeapons",(player: alt.Player)=>{
	native.giveWeaponToPed(player.scriptID, 453432689, 999, false, true);
	native.giveWeaponToPed(player.scriptID, 961495388, 999, false, false);
	native.giveWeaponToPed(player.scriptID, 2138347493, 999, false, false);
	native.giveWeaponToPed(player.scriptID, 4024951519, 999, false, false);
	native.giveWeaponToPed(player.scriptID, 4024951519, 999, false, false);
	native.setPedCanBeKnockedOffVehicle(player.scriptID, 1);
	native.giveWeaponComponentToPed(player.scriptID,0x1B06D571,0x359B7AAE)
	
});

alt.onServer("redoBikeFlag",()=>{

		native.setPedCanBeKnockedOffVehicle(alt.Player.local.scriptID, 1);	
		
});


alt.onServer("fixVehicle",(vehicle: alt.Vehicle)=>{
	native.setVehicleFixed(vehicle.scriptID);
	
});

alt.onServer('suicidePlayer', (player:alt.Player) => {
	native.giveWeaponToPed(player.scriptID, 453432689, 1, false, true);
  
	alt.setTimeout(() => {
		native.taskPlayAnim(player.scriptID, 'mp_suicide', 'pistol', 8.0, 1.0, -1, 2, 0, false, false, false);
	}, 500);
  
	alt.setTimeout(() => {
		native.setPedShootsAtCoord(player.scriptID, 0, 0, 0, true);
		native.startScreenEffect('DeathFailNeutralIn', 2000, false);
		alt.emitServer('killSelf');
	}, 1250);
});

// let blips = new Map();
// let canDo: boolean = false;
let blips: alt.PointBlip[] = [];




// alt.onServer("updatePlayerList",(list: alt.Player[])=>{
// 	blips.forEach((blip: alt.PointBlip, player: alt.Player)=>{
// 		blip.destroy;
// 	});
// 	blips.clear();
// 	list.forEach((player: alt.Player)=>{
// 		let blip: alt.PointBlip = new alt.PointBlip(player.pos.x, player.pos.y, player.pos.z)
// 		blip.sprite = 126;
// 		blip.color = 2;
// 		blip.scale = 0.75;
// 		blips.set(player, blip)
// 	});

// });

alt.onServer("updateBlips", (blipVectorList: Array<Array<number>>)=>{
	if(blips === undefined || blips.length == 0){
		if (blipVectorList !== undefined || blipVectorList.length != 0){
			blipVectorList.forEach((blipVector: number[])=>{
				let blip = new alt.PointBlip(blipVector[0],blipVector[1],blipVector[2]);
				blip.sprite = 126;
				blip.color = 2;
				blip.scale = 0.75;
				blips.push(blip);
			});
		
		}
	} else {
		blips.forEach((blip: alt.PointBlip)=>{
			blip.destroy();
		});
		blips.length = 0;
		if (blipVectorList !== undefined || blipVectorList.length != 0){
			blipVectorList.forEach((blipVector: number[])=>{
				let blip = new alt.PointBlip(blipVector[0],blipVector[1],blipVector[2]);
				blip.sprite = 126;
				blip.color = 2;
				blip.scale = 0.75;
				blips.push(blip);
			});
		}
	}
})

 