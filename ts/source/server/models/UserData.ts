import { Entity, PrimaryGeneratedColumn, Column, UpdateDateColumn, CreateDateColumn } from 'typeorm-esm'
import {PersonalVehicle} from './PersonalVehicles'
import alt from "alt"

@Entity({name: "users"})
export class UserData {

	@PrimaryGeneratedColumn()
	id: number

	@Column({ type: 'varchar', default: '' })
	username: string

	@Column({ type: 'varchar', default: '' })
	password: string

	@Column({ type: 'integer', default: 1000 })
	money: number

	@UpdateDateColumn()
	updatedAt: Date

	@CreateDateColumn()
	createdAt: Date

	personalVehicles: PersonalVehicle[]
	playerHandle: alt.Player

	constructor(username: string, password: string) {
		this.username = username;
		this.password = password;
	}
	
}