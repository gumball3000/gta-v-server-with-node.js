"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var typeorm_esm_1 = require("typeorm-esm");
var ExampleModel = /** @class */ (function () {
    function ExampleModel() {
    }
    __decorate([
        typeorm_esm_1.PrimaryGeneratedColumn()
    ], ExampleModel.prototype, "id");
    __decorate([
        typeorm_esm_1.Column({ type: 'varchar', "default": '' })
    ], ExampleModel.prototype, "username");
    __decorate([
        typeorm_esm_1.UpdateDateColumn()
    ], ExampleModel.prototype, "updatedAt");
    __decorate([
        typeorm_esm_1.CreateDateColumn()
    ], ExampleModel.prototype, "createdAt");
    ExampleModel = __decorate([
        typeorm_esm_1.Entity()
    ], ExampleModel);
    return ExampleModel;
}());
exports.ExampleModel = ExampleModel;
