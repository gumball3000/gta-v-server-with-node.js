import { Entity, PrimaryGeneratedColumn, Column, UpdateDateColumn, CreateDateColumn } from 'typeorm-esm'

@Entity({name: "spawnpoints"})
export class SpawnPoint {

	@PrimaryGeneratedColumn()
	id: number

	@Column({ type: 'float'})
    posx: number
    
    @Column({ type: 'float'})
    posy: number
    
    @Column({ type: 'float'})
    posz: number
    



    constructor(posx: number, posy: number, posz: number) {
        this.posx = posx;
        this.posy = posy;
        this.posz = posz;

	}
}