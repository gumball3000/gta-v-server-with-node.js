import { Entity, PrimaryGeneratedColumn, Column, UpdateDateColumn, CreateDateColumn } from 'typeorm-esm'

@Entity({name: "personalvehicles"})
export class PersonalVehicle {

	@PrimaryGeneratedColumn()
    id: number

    @Column({ type: 'varchar', default: '' })
	model: string

	@Column({ type: 'integer' })
	playerId: number

	@Column({ type: 'float'})
    posx: number
    
    @Column({ type: 'float'})
    posy: number
    
    @Column({ type: 'float'})
    posz: number
    
    @Column({ type: 'float'})
    rotx: number
    
    @Column({ type: 'float'})
    roty: number
    
    @Column({ type: 'float'})
	rotz: number

	@UpdateDateColumn()
	updatedAt: Date

	@CreateDateColumn()
    createdAt: Date
    

    constructor(playerId: number, model: string, posx: number, posy: number, posz: number, 
        rotx: number, roty: number, rotz: number) {
        this.playerId = playerId;
        this.model = model;
        this.posx = posx;
        this.posy = posy;
        this.posz = posz;
        this.rotx = rotx;
        this.roty = roty;
        this.rotz = rotz;
	}
	
}