import { Entity, PrimaryGeneratedColumn, Column, UpdateDateColumn, CreateDateColumn } from 'typeorm-esm'

@Entity({name: "servervehicles"})
export class ServerVehicle {

	@PrimaryGeneratedColumn()
    id: number

    @Column({ type: 'varchar', default: '' })
	model: string

	@Column({ type: 'float'})
    posx: number
    
    @Column({ type: 'float'})
    posy: number
    
    @Column({ type: 'float'})
    posz: number
    
    @Column({ type: 'float'})
    rotx: number
    
    
    @Column({ type: 'float'})
    roty: number
    
    @Column({ type: 'float'})
	rotz: number

	@UpdateDateColumn()
	updatedAt: Date

	@CreateDateColumn()
    createdAt: Date

    vehicleId: number
    

    constructor(model: string, posx: number, posy: number, posz: number, 
        rotx: number, roty: number, rotz: number) {
        this.model = model;
        this.posx = posx;
        this.posy = posy;
        this.posz = posz;
        this.rotx = rotx;
        this.roty = roty;
        this.rotz = rotz;
	}
	
}