import alt, { Vehicle, Vector3 } from 'alt'
import chat from 'chat'
import exampleFunction from './functions/exampleFunction'
import {UserData} from '../../models/UserData'
import {PersonalVehicle} from '../../models/PersonalVehicles'
import {ServerVehicle} from '../../models/ServerVehicles'
import { getConnection,getRepository} from 'typeorm-esm'
import { SpawnPoint } from '../../models/SpawnPoints'
let serverVehicles: ServerVehicle[];
export default async () => {
	await exampleFunction()
	alt.log('exampleModule inited')
	spawnPoints = await getRepository(SpawnPoint).createQueryBuilder("spawnpoints").getMany();
	serverVehicles = await getRepository(ServerVehicle).createQueryBuilder("servervehicles").getMany();
	 serverVehicles.forEach(vehicle=>{
		let v: Vehicle = new Vehicle(parseInt(vehicle.model),vehicle.posx,vehicle.posy,vehicle.posz,vehicle.rotx,vehicle.roty,vehicle.rotz);
		spawnedServerVehicles.set(v.id,vehicle);
		serverVehicleList.push(v);

	});

	setInterval(() => {
		let newArray: Vehicle[] = [];
		serverVehicleList.forEach(veh=>{
			 if (veh.driver === undefined || veh.driver === null ){
			let vehicle: ServerVehicle=spawnedServerVehicles.get(veh.id);
			spawnedServerVehicles.delete(veh.id);
			veh.destroy();
			let v: Vehicle = new Vehicle(parseInt(vehicle.model),vehicle.posx,vehicle.posy,vehicle.posz,vehicle.rotx,vehicle.roty,vehicle.rotz);

			newArray.push(v);
			spawnedServerVehicles.set(v.id,vehicle);
			 } else {
				 newArray.push(veh);
			}
		});
		serverVehicleList = newArray;
	}, 120000);


	// setInterval(() => {
	// 	serverVehicleList.forEach(veh=>{
	// 		 if (veh.driver === undefined || veh.driver === null ){
	// 			 let pos = new alt.Vector3(spawnedServerVehicles.get(veh.id).posx,spawnedServerVehicles.get(veh.id).posy,
	// 			 spawnedServerVehicles.get(veh.id).posz);
	// 			 let rot = new alt.Vector3(spawnedServerVehicles.get(veh.id).rotx,spawnedServerVehicles.get(veh.id).roty,
	// 			 spawnedServerVehicles.get(veh.id).rotx)
	// 			veh.pos = pos;
	// 			veh.rot = rot;
			


	// 		}
	// 	});
	// }, 20000);

	setInterval(() => {
		let newArray: Vehicle[] = [];
		temporaryVehicles.forEach(veh=>{
			 if (veh.driver === undefined || veh.driver === null ){
			veh.destroy();
			 } else {
				 newArray.push(veh);
			}
		});
		temporaryVehicles = newArray;
	}, 120000);

	setInterval(() => {
		let playerPositionList:Array<Array<number>> = [];
		loggedUsers.forEach((user:UserData)=>{
			let positionVector = [user.playerHandle.pos.x, user.playerHandle.pos.y,user.playerHandle.pos.z];
			playerPositionList.push(positionVector);
		});
		alt.emitClient(null, "updateBlips", playerPositionList);
	}, 100);


}

export function updatePlayerListForClients(){
	let list: alt.Player[] = [];
	loggedUsers.forEach((user:UserData)=>{
		list.push(user.playerHandle);
	})
	alt.emitClient(null,"updatePlayerList", list);
}

export function updatePlayerListForClient(player: alt.Player){
	let list: alt.Player[] = [];
	loggedUsers.forEach((user:UserData)=>{
		list.push(user.playerHandle);
	})
	alt.emitClient(player,"updatePlayerList", list);
}



let loggedUsers = new Map();
let spawnedVehicles = new Map();
let spawnedServerVehicles = new Map();
let playerVehicleList: Vehicle[];
let serverVehicleList: Vehicle[]= [];
let pendingUsers = new Map();
let spawnPoints: SpawnPoint[];

alt.onClient("tpToBlip", (player: alt.Player, coords: alt.Vector3) => {
	player.pos = coords;
});

alt.on("playerConnect", joinServer);
alt.on('playerDeath', (player: alt.Player) => {
	let sp = spawnPoints[Math.floor(Math.random() * spawnPoints.length)];
	player.spawn(sp.posx,sp.posy,sp.posz, 2000);
  });

chat.registerCmd("register", register);
chat.registerCmd("login", login);
chat.registerCmd('suicide', (player:alt.Player) => {
	alt.emitClient(player, 'suicidePlayer', player);
});

chat.registerCmd('help', (player:alt.Player) => {
	chat.send(player, "{FF5500}Commands are /help, /wep, /veh, /fixveh, /skin, /tp \"id\",");
	chat.send(player, "{FF5500}/carcolor, /mod and /suicide, /godmode");
	chat.send(player, "{FF5500}VoiceChat is ON, press N to talk");
});


chat.registerCmd('fixveh', (player:alt.Player) => {
	if (player.vehicle !== undefined && player.vehicle !== null){
		if (player.vehicle.driver === player){
		alt.emitClient(player, "fixVehicle", player.vehicle); 
		} else {
			chat.send(player, "You are not the driver");
		}
	} else {
		chat.send(player, "You are not inside a vehicle");
	}
});

// chat.registerCmd("savespawn", async (player: alt.Player) => {
// 	let sp = new SpawnPoint(player.pos.x, player.pos.y, player.pos.z);
// 	await getConnection().transaction(async transactionalEntityManager => {
// 		await transactionalEntityManager.save(sp);});
// 		spawnPoints.push(sp);
// 		chat.send(player, "Point Saved");
	
// });

// chat.registerCmd("park", park);

// async function park(player: alt.Player) {
//     if (player.vehicle !== undefined) {
//         chat.send(player, "this got called");
//         let user: UserData = loggedUsers.get(player.id);
// 		let veh = new ServerVehicle(player.vehicle.model.toString(), player.vehicle.pos.x, player.vehicle.pos.y, player.vehicle.pos.z,
// 			player.vehicle.rot.x, player.vehicle.rot.y, player.vehicle.rot.z);
//         await getConnection().transaction(async transactionalEntityManager => {
//             await transactionalEntityManager.save(veh);
// 			chat.send(player, "car parked");
//         });
//     }
// }

// chat.registerCmd("buycar", buyCar);

// async function buyCar(player: alt.Player) {
//     if (player.vehicle !== undefined) {
//         chat.send(player, "this got called");
//         let user: UserData = loggedUsers.get(player.id);
// 		let veh = new PersonalVehicle(user.id, player.vehicle.model.toString(), player.vehicle.pos.x, player.vehicle.pos.y, player.vehicle.pos.z,
// 			player.vehicle.rot.x, player.vehicle.rot.y, player.vehicle.rot.z);
//         await getConnection().transaction(async transactionalEntityManager => {
//             await transactionalEntityManager.save(veh);
// 			chat.send(player, "You bought this car");
// 			user.personalVehicles.push(veh);
//         });
//     }
// }

async function updatePlayerMoney(player: alt.Player, amount: number, operation: number) {
	let user: UserData = loggedUsers.get(player.id);
	if (operation === 0){
		user.money += amount;
	} else {
		user.money -= amount;
	}
}

async function saveUserData(user: UserData) {
	await getConnection().transaction(async transactionalEntityManager => {
		await transactionalEntityManager.save(user);});
}


let temporaryVehicles: alt.Vehicle[] = [];
chat.registerCmd("veh",  (player: alt.Player, arg: any) => {
	try {
	let veh : Vehicle = new Vehicle(arg[0], player.pos.x, player.pos.y, player.pos.z,0,0,0);
	temporaryVehicles.push(veh);
	} catch {
		chat.send(player, "Wrong vehicle type");	
	}
});

chat.registerCmd("tp",  (player: alt.Player, arg: string[]) => {
	if (loggedUsers.has(parseInt(arg[0]))){
		let tp: UserData = loggedUsers.get(parseInt(arg[0]));
		player.pos = tp.playerHandle.pos;
	} else {
		chat.send(player, "Player is not logged in");
	}
});





// async function joinServer(player : alt.Player) {
// 	let username = player.name;
// 	alt.log(`${username}[${player.id}] has joined`)
// 	username.toLowerCase();
// 	let user: UserData = await getRepository(UserData)
// 		.createQueryBuilder("users")
// 		.where("users.username = :username", { username: username})
// 		.getOne();
// 		//alt.log(`username: ${user.username}`)
// 		if (typeof user === "undefined"){
// 			chat.send(player,'Please create an account using /register \"password\"');
// 		} else {
// 			chat.send(player,'You already have an account, please login using /login \"password\"');
// 			pendingUsers.set(player.id, true);
// 		}
	
// }

// async function register(player: alt.Player, arg:string) {
// 	if (pendingUsers.get(player.id)){
// 		chat.send(player, "You already registered");
// 	} else {
// 		if (arg[0]){	
// 			let username: string = player.name;
// 			username.toLowerCase();
// 			let user = new UserData(username, arg);
// 				await getConnection().transaction(async transactionalEntityManager => {
// 				await transactionalEntityManager.save(user);
// 				chat.send(player, `Welcome! Your password is \"${arg}\"`);
// 				chat.send(player, "You registered, please login using /login \"password\"");
// 				pendingUsers.set(player.id, true)
// 			});
// 		} else {
// 		chat.send(player, "Invalid password");
// 	}
// }
// }


// export function login(player:alt.Player, arg:string){
// 	(async () => {
// 		if (!loggedUsers.has(player.id)){
// 			let username = player.name;
// 			let user: UserData = await getRepository(UserData)
// 				.createQueryBuilder("users")
// 				.where("users.username = :username", { username: username.toLowerCase()})
// 				.getOne();

// 			if (user.password == arg){
// 				spawnPlayer(player);
// 				chat.broadcast(`${player.name}[${player.id}] joined the server`)
// 				user.playerHandle = player;
// 				loggedUsers.set(player.id, user);
// 				updatePlayerListForClients();
// 				user.personalVehicles = await getRepository(PersonalVehicle)
// 				.createQueryBuilder("personalvehicles")
// 				.where("personalvehicles.playerId = :playerId", { playerId: user.id})
// 				.getMany();
// 				chat.send(player, `You logged in with id ${player.id}`);
// 				chat.send(player, "{FF5500}Commands are /help, /wep, /veh, /fixveh, /skin, /tp \"id\", /carcolor and /suicide")
// 				chat.send(player, "{FF5500}VoiceChat is ON, press N to talk")
// 				pendingUsers.set(player.id, false);
// 				alt.emitClient(player, "giveWeapons", player);

// 				// user.personalVehicles.forEach(vehicle => {
// 				// 	alt.log(`found vehicle ${vehicle.model} in db`);
// 				// });

// 				spawnPlayerVehicles(player, user.personalVehicles);
// 			} else {
// 				chat.send(player, "Wrong password");
// 			}
// 		} else {
// 			chat.send(player, "You are already logged in")		
// 		}
// 	})();
// }

export function spawnPlayer(player: alt.Player){
	// player.spawn(-275.522,6635.835,7.425, 0);
	let sp = spawnPoints[Math.floor(Math.random() * spawnPoints.length)];
	player.spawn(sp.posx,sp.posy,sp.posz, 0);
}

async function spawnPlayerVehicles(player: alt.Player, vehicles: PersonalVehicle[]){
	playerVehicleList = [];
	vehicles.forEach(vehicle => {
		let v: Vehicle = new Vehicle(parseInt(vehicle.model),vehicle.posx,vehicle.posy,vehicle.posz,vehicle.rotx,vehicle.roty,vehicle.rotz);
		playerVehicleList.push(v);

	});
	spawnedVehicles.set(player.id, playerVehicleList);
}

export function destroyPlayerVehicles(player: alt.Player){
	let vehicles:Vehicle[]=spawnedVehicles.get(player.id);
	for (let vehicle of vehicles){
		vehicle.destroy();
	}
	spawnedVehicles.delete(player.id);

// for(var vehicle: PersonalVehicle of spawnedVehicles: PersonalVehicle[]){

// }
	// spawnedVehicles.forEach((vehicle: PersonalVehicle) => {
	// 	if (vehicle.owner === player.id){

	// 	}
	// })

	// let newCarMap = spawnedVehicles;
	// newCarMap.forEach((vehicle: alt.Vehicle, playerId)=>{
	// 	if (player.id === playerId){
	// 		vehicle.destroy();
	// 		spawnedVehicles.delete(vehicle);
	// 	}
	// });
}
alt.on('playerDisconnect', (player: alt.Player) => {
	alt.log(`${player.name} has left`);
	chat.broadcast(`${player.name}[${player.id}] left the server`)
	if (loggedUsers.has(player.id)){
	destroyPlayerVehicles(player);
	saveUserData(loggedUsers.get(player.id));
	loggedUsers.delete(player.id);
	pendingUsers.set(player.id, false);
	updatePlayerListForClients();
	}
});

alt.onClient('killSelf', (player:alt.Player) => {
	player.health = 0;
});


alt.onClient('register', (player:alt.Player, pwd:string) => {
	register(player, pwd);
});


async function register(player: alt.Player, password:string) {
	if (pendingUsers.get(player.id)){
		chat.send(player, "You already registered");
	} else {	
			let username: string = player.name;
			username.toLowerCase();
			let user = new UserData(username, password);
				await getConnection().transaction(async transactionalEntityManager => {
				await transactionalEntityManager.save(user);
				chat.send(player, `Welcome! Your password is \"${password}\"`);
				chat.send(player, "You registered, please login using /login \"password\"");
				pendingUsers.set(player.id, true)
				alt.emitClient(player, "registerSuccess")
			});

	}
}

alt.onClient('login', (player:alt.Player, pwd:string) => {
	login(player, pwd);
});

async function joinServer(player : alt.Player) {
	let username = player.name;
	alt.log(`${username}[${player.id}] has joined`)
	username.toLowerCase();
	let user: UserData = await getRepository(UserData)
		.createQueryBuilder("users")
		.where("users.username = :username", { username: username})
		.getOne();
		alt.emitClient(player, 'disableChat');
		//alt.log(`username: ${user.username}`)
		if (typeof user !== "undefined"){
			alt.emitClient(player, "userHasAccount", user.password)
			pendingUsers.set(player.id, true);
		}
	
}

export function login(player:alt.Player, arg:string){
	(async () => {
		if (!loggedUsers.has(player.id)){
			let username = player.name;
			let user: UserData = await getRepository(UserData)
				.createQueryBuilder("users")
				.where("users.username = :username", { username: username.toLowerCase()})
				.getOne();

			if (user.password == arg){
				spawnPlayer(player);
				chat.broadcast(`${player.name}[${player.id}] joined the server`)
				user.playerHandle = player;
				loggedUsers.set(player.id, user);
				updatePlayerListForClients();
				user.personalVehicles = await getRepository(PersonalVehicle)
				.createQueryBuilder("personalvehicles")
				.where("personalvehicles.playerId = :playerId", { playerId: user.id})
				.getMany();
				chat.send(player, `You logged in with id ${player.id}`);
				chat.send(player, "{FF5500}Commands are /help, /wep, /veh, /fixveh, /skin, /tp \"id\",");
				chat.send(player, "{FF5500}/carcolor, /mod and /suicide, /godmode");
				chat.send(player, "{FF5500}VoiceChat is ON, press N to talk");
				pendingUsers.set(player.id, false);
				alt.emitClient(player, "giveWeapons", player);
				alt.emitClient(player, 'enableChat');

				// user.personalVehicles.forEach(vehicle => {
				// 	alt.log(`found vehicle ${vehicle.model} in db`);
				// });

				spawnPlayerVehicles(player, user.personalVehicles);
				alt.emitClient(player, "loginSuccessful")
			} else {
				chat.send(player, "Wrong password");
			}
		} else {
			chat.send(player, "You are already logged in")		
		}
	})();
}






