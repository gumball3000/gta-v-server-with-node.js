import * as alt from 'alt'
import chalk from 'chalk'
import { getConnection } from './database'

import exampleModule from './modules/exampleModule/init'
import exampleModule2 from './modules/exampleModule2/init'
(async () => {
	await getConnection()

	await exampleModule()
	await exampleModule2()
	alt.log(chalk.yellowBright('All modules loaded'))
})()