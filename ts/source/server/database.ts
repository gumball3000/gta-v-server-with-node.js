import * as alt from 'alt'
import { createConnection, Connection, ConnectionOptions } from 'typeorm-esm'
import path from 'path'
import fs from 'fs-extra'
import chalk from 'chalk'

import { UserData } from './models/UserData'
import { PersonalVehicle } from './models/PersonalVehicles'
import { ServerVehicle } from './models/ServerVehicles'
import { SpawnPoint } from './models/SpawnPoints'


const config: ConnectionOptions = {
	...fs.readJsonSync(path.join(alt.rootDir, 'ormconfig.json')),
	entities: [
		UserData,
		PersonalVehicle,
		ServerVehicle,
		SpawnPoint
	]
}

let conn: Connection = null


export const getConnection = async () => {
	try {
		if (conn === null) {
			const connection = await createConnection(config)
			conn = connection
			alt.log(chalk.greenBright('Database connected successfully'))
			conn.createEntityManager
		}
		return conn
	} catch (err) {
		console.log(err)
	}
	
}