import * as alt from 'alt';
import * as game from 'natives';

const view = new alt.WebView("http://resources/accounts/client/registerLogin.html");

alt.showCursor(true);
view.focus();
let loginCamera;
alt.toggleGameControls(false);

alt.on('connectionComplete', () => {
    game.freezeEntityPosition(alt.Player.local.scriptID, true);
    setStaticCamera();
})



view.on("register", (password)=>{
    alt.emitServer("register", password);
})

view.on("login", (password)=>{
    alt.emitServer("login", password);
})

alt.onServer("registerSuccess", ()=>{
    view.emit("registerSuccess"); 
})

alt.onServer("userHasAccount", (password)=>{
    view.emit("userHasAccount",password ); 
})


alt.onServer("loginSuccessful", ()=>{
    game.clearFocus();
    game.clearHdArea();
    view.destroy(); 
    game.renderScriptCams(false, false, 0, true, false);
    game.setCamActive(loginCamera, false);
    game.destroyCam(loginCamera, false);
    alt.showCursor(false);
    alt.toggleGameControls(true);
    game.freezeEntityPosition(alt.Player.local.scriptID, false);
})

function setStaticCamera()
{
    loginCamera = game.createCam('DEFAULT_SCRIPTED_CAMERA', true);
    game.setCamCoord(loginCamera, 619.84912109375,1143.0748291015625,353.6922302246094);
    game.setCamRot(loginCamera, -6.675822734832764,0,-55.17519760131836);
    game.setCamFov(loginCamera, 65);
    
    game.setCamActive(loginCamera, true);
    game.renderScriptCams(true, false, 0, true, false);
    game.setFocusArea(619.84912109375,1143.0748291015625,353.6922302246094, 5, 0.0, 0.0, 0.0);    
    game.setHdArea(619.84912109375,1143.0748291015625,353.6922302246094, 5, 30);
}



