let mode = "register"
let label1 = document.getElementById("label1");
let label2 = document.getElementById("label2");
let button = document.getElementById("button");
let userPassword;
label1.innerHTML = "You do not have an account, please register below.";
button.innerHTML = "Register"

function register(){
    let password = document.getElementById("password").value;
}

function buttonAction(){
    let password = document.getElementById("password").value;
    if (mode === "register"){
        if (password.length >= 6 && password.length <= 12){
        alt.emit("register", password);
        userPassword = password;
        button.disabled = true;
        } else {
            label2.innerHTML = "Password must have 6-12 characters"
            document.getElementById("password").value = ""; 
        }
    } else {
        if (password === userPassword){
        button.disabled = true;
        alt.emit("login", password); 
        } else {
            label2.innerHTML = "Wrong password"
            document.getElementById("password").value = "";
        }
    }
}


if ("alt" in window){
    alt.on("registerSuccess", ()=>{
        mode = "login";
        label1.innerHTML = "Thank you for Registering, please login."
        button.innerHTML = "Login"
        //userPassword = pwd;
        button.disabled = false;
    });
    alt.on("userHasAccount", (pwd)=>{
        mode = "login";
        label1.innerHTML = "You already have an account, please login."
        button.innerHTML = "Login"
        userPassword = pwd;
    });
}

document.addEventListener("DOMContentLoaded", (e)=>{
    alt.emit("youCanLoadTheCamera")
});
