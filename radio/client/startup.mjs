import * as alt from 'alt';
let view = new alt.WebView("http://resources/radio/client/index.html");
let radio = false;

alt.onServer("playRadio", ()=>{
    //alt.log("radio on")
    if (radio){
    view.emit("playAudio");
    }
});

alt.onServer("stopRadio", ()=>{
    //alt.log("radio off")
    if (radio){
    view.emit("stopAudio");
    }
})

alt.onServer("radioOn", ()=>{
    radio = true;
})

alt.onServer("radioOff", ()=>{
     radio = false;
})




