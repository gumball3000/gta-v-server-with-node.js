var audio = new Audio("http://stream.radioreklama.bg/nrj_low.ogg");
audio.loop = false;
audio.preload = "none"
audio.volume = 0.35;

// function playAudio() {
//     audio = new Audio("http://stream.radioreklama.bg/nrj_low.ogg");
//     var playPromise = audio.play();
//     if (playPromise !== undefined){
//         playPromise.then(()=>{
//             audio.play();
//         })
//         .catch(error => {
//             playAudio();
//           });
      
//     }
	
// }

// function stopAudio() {
//     audio.pause();
//     audio = new Audio("http://stream.radioreklama.bg/nrj_low.ogg");
// }

if ('alt' in window) {
    alt.on('playAudio', playAudio);
    alt.on('stopAudio', stopAudio);
}

var isPlaying = false;

// On video playing toggle values
audio.onplaying = function() {
    isPlaying = true;
};

// On video pause toggle values
audio.onpause = function() {
    isPlaying = false;
};

// Play video function
function playAudio() {      
    if (audio.paused && !isPlaying) {
        audio.load();
        audio.play();
    }
} 

// Pause video function
function stopAudio() {     
    if (!audio.paused && isPlaying) {
        audio.pause();
    }
}