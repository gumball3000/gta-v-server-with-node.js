import * as alt from 'alt';
import * as chat from 'chat';


alt.on('playerEnteredVehicle', (player, vehicle, seat) => {

    alt.emitClient(player, "playRadio");
    if (player.messageSent === undefined){
    chat.send(player, "You can use /radio on/off to play radio while driving");
    }
    player.messageSent = true;
});

alt.on('playerLeftVehicle', (player, vehicle, seat) => {
    alt.emitClient(player, "stopRadio");
});

chat.registerCmd("radio",  (player, args) => {
    if (args[0] != "on"){
        if (args[0] != "off"){
       
            return;
        }

    }
    if (args[0] == "on"){
        alt.emitClient(player, "radioOn"); 
        if (player.vehicle !== undefined && player.vehicle !== null){
            alt.emitClient(player, "playRadio"); 
        }
    } else {
        alt.emitClient(player, "stopRadio");
        alt.emitClient(player, "radioOff");
    }
});