import * as alt from 'alt';
import * as NativeUI from 'includes/NativeUIMenu/NativeUI.mjs';
import * as native from "natives";

let godmode = false;
alt.on("update",()=>{
	if (godmode){
		native.setEntityHealth(alt.Player.local.scriptID, 200)
	}
})
alt.onServer("godMode", ()=>{
	godmode = !godmode
})

// const categoryTitles = 
// [
// 	"Spoiler",
// 	"Front Bumper",
// 	"Rear Bumper",
// 	"Side Skirts",
// 	"Exhaust",
// 	"Rollcage",
// 	"Grille",
// 	"Bonnet",
// 	"Fenders and Arches",
// 	"Fenders",
// 	"Roof",
// 	"Engine",
// 	"Brakes",
// 	"Transmission",
// 	"Horn",
// 	"Suspension",
// 	"Armor",
// 	"",
// 	"",
// 	"",
// 	"",
// 	"",
// 	"Headlights",
// 	"",
// 	"",
// 	"Plate Holders",
// 	"Vanity Plates",
// 	"Interior Trim",
// 	"Ornaments",
// 	"Interior Dash",
// 	"Dials",
// 	"Door Speakers",
// 	"Leather Seats",
// 	"Steering Wheels",
// 	"Column Shifters",
// 	"Plaques",
// 	"ICE",
// 	"Speakers",
// 	"Hydraulics",
// 	"Engine Block",
// 	"Air Filters",
// 	"Strut Braces",
// 	"Arch Covers",
// 	"Aerials",
// 	"Exterior Trim",
// 	"Tank",
// 	"Windows",
// 	"",
// 	"Livery"
// ];

const categoryTitles = 
[
    'SPOILER',
    'FRONTBUMPER',
    'REARBUMPER',
    'SIDESKIRT',
    'EXHAUST',
    'CHASSIS', 
    'GRILLE',
    'HOOD',
    'FENDER',
    'RIGHTFENDER',
    'ROOF',
    'ENGINE',
    'BRAKES',
    'TRANSMISSION',
    'HORNS',
    'SUSPENSION',
    'ARMOR',
    'UNK17',
    'TURBO',
    'UNK19',
    'TIRESMOKE',
    'UNK21',
    'XENONLIGHTS',
    'FRONTWHEELS',
    'BACKWHEELS', 
    'PLATEHOLDER',
    'VANITY_PLATES',
    'TRIM',
    'ORNAMENTS',
    'DASHBOARD',
    'DIAL',
    'DOOR_SPEAKER',
    'SEATS',
    'STEERINGWHEEL',
    'SHIFTER_LEAVERS',
    'PLAQUES',
    'SPEAKERS',
    'TRUNK',
    'HYDRULICS',
    'ENGINE_BLOCK',
    'AIR_FILTER',
    'STRUTS',
    'ARCH_COVER',
    'AERIALS',
    'TRIM',
    'TANK',
    'WINDOWS',
    'UNK47',
    'LIVERY'
]

// main menu
let mainMenu = null;

let curCategory = -1;
let transition = false;

let menus = {};

function createTuningMenu(vehicle)
{
	curCategory = -1;
	
	if(mainMenu)
	{
		mainMenu.Visible = false;
	}
	
	if(menus.hasOwnProperty(vehicle.model))
	{
		mainMenu = menus[vehicle.model];
		return;
	}
	native.setVehicleModKit(vehicle.scriptID, 0);
	
	mainMenu = new NativeUI.Menu("Vehicle Tuning", "", new NativeUI.Point(300, 200));
	mainMenu.Visible = false;
	
	menus[vehicle.model] = mainMenu;
	
	mainMenu.vehicleModel = vehicle.model;

	mainMenu.ItemSelect.on((item, index) => {
		mainMenu.Visible = false;
		curCategory = index;
		categoryMenus[index].Visible = true;
		transition = true;
	});

	let categoryMenus = [];

	// categories
	for (let i = 0; i < categoryTitles.length; i++) 
	{
        let numMods = native.getNumVehicleMods(vehicle.scriptID, i);
        alt.log("numods " + numMods)
		
		if(numMods > 0 && categoryTitles[i].length > 0)
		{
			mainMenu.AddItem(new NativeUI.UIMenuItem(categoryTitles[i], ""));

			let categoryMenu = new NativeUI.Menu(categoryTitles[i], "", new NativeUI.Point(300, 200));
			categoryMenu.Visible = false;

			categoryMenu.ItemSelect.on((item, index) => {
				if (!transition) alt.emitServer("vtuning_set", item.modType, item.modIndex);
				transition = false;
			});

			categoryMenu.MenuClose.on(() => {
				curCategory = -1;
				mainMenu.Visible = true;
			});
			
			categoryMenu.modType = i;

			for(let modIndex = 0; modIndex < numMods; modIndex++)
			{
                let vehicleName = native.getLabelText(native.getModTextLabel(vehicle.scriptID, i, modIndex));
                
				let vehicleItem = new NativeUI.UIMenuItem(vehicleName == "NULL" ? `${categoryTitles[i]} #${modIndex}` : vehicleName, "");
				vehicleItem.modType = i;
				vehicleItem.modIndex = modIndex;
				categoryMenu.AddItem(vehicleItem);
			}

			categoryMenus.push(categoryMenu);
		}
	}
}

function toggleMenu()
{
   if (curCategory > -1) 
	{
        categoryMenus[curCategory].Visible = !categoryMenus[curCategory].Visible;
    }
	else 
	{
		if(!mainMenu || !mainMenu.Visible)
		{		
			let vehicle = alt.Player.local.vehicle;
			
			if(vehicle)
			{				
				if(!mainMenu
					|| mainMenu.vehicleModel !== vehicle.model)
				{
					createTuningMenu(vehicle);
				}
				
				mainMenu.Visible = true;
			}
			// else
			// {
			// 	mp.game.graphics.notify("~R~You should be in a vehicle to access this menu.");
			// }
		}	
		else
		{
			mainMenu.Visible = false;
		}
    }	
}


alt.onServer("modVehicle", ()=>{
	toggleMenu();
})
// alt.on('keydown', (key) => {
//     if (key === 0x4D) {
// 		//M Key
// 	toggleMenu()
//     }
// }); 