import * as alt from "alt"
import * as chat from "chat"
let godmode = false;

alt.onClient("vtuning_set", (player, modType, modIndex) =>{
	player.vehicle.modKit = 1;
    if (player.vehicle !== undefined && player.vehicle !== null){
        player.vehicle.setMod(modType, modIndex);
    }
})


alt.on('playerEnteredVehicle', (player, vehicle, seat) => {

    if (player.messageSent === undefined){
        chat.send(player, "You can mod this vehicle using /mod");
    }
    player.messageSent = true;
});


chat.registerCmd('mod', (player) => {
	if (player.vehicle !== undefined && player.vehicle !== null){
		if (player.vehicle.driver === player){
		alt.emitClient(player, "modVehicle"); 
		} else {
        chat.send(player, "You are not the driver");
		}
	} else {
		chat.send(player, "You are not inside a vehicle");
	}
});

chat.registerCmd('godmode', (player) => {
    if (player.godmode){
        chat.send(player, "Godmode turned off"); 
        alt.emitClient(player, "godMode"); 
        player.godmode = !player.godmode
    } else {
        chat.send(player, "Godmode turned on"); 
        alt.emitClient(player, "godMode"); 
        player.godmode = true
    }
});
