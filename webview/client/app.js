document.addEventListener("DOMContentLoaded", (e)=>{
    hideSpeedo();
});


if ("alt" in window){
    alt.on("showSpeedo", showSpeedo);
    alt.on("hideSpeedo", hideSpeedo);
    alt.on("update", update);
}

function update(speed, gas)
{			
    document.getElementsByClassName('kmh')[0].innerHTML = speed + "KM/H";
    document.getElementsByClassName('inline1')[0].style.width = gas + "%";
}

function hideSpeedo() {
    document.getElementsByTagName('body')[0].style.display = "none";
}

function showSpeedo() {
    document.getElementsByTagName('body')[0].style.display = "block";
}