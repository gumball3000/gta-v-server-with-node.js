import * as alt from 'alt';

const view = new alt.WebView("http://resources/webview/client/index.html");

// alt.onServer("showTheSpeedo",(player)=>{
//     view.emit("showSpeedo");
// });

// alt.onServer("hideTheSpeedo",(player)=>{
//     view.emit("hideSpeedo");
// });
let showingspeedo = false;

alt.on("update", ()=>{
    if (alt.Player.local.vehicle === undefined || alt.Player.local.vehicle === null){
        if (showingspeedo === true){
        view.emit("hideSpeedo"); 
        showingspeedo = false;
        }
    } else {
        if (showingspeedo === false){
        view.emit("showSpeedo");
        showingspeedo = true;
        }
        let vel1 = alt.Player.local.vehicle.speed * 3.6;
        let vel = (vel1).toFixed(0);
        view.emit("update",vel,100);   
    }
})


