import * as alt from "alt"
import * as native from "natives"

alt.on('update', () => {
    alt.Player.all.forEach((player) => {
        // Don't draw local player.
        if (player == alt.Player.local)
            return;

        // Determine the distance we want to draw from.
        let distanceFromLocal = Distance(player.pos, alt.Player.local.pos);
        if (distanceFromLocal >= 25)
            return;

        // Make sure player is on screen.
        let result = native.getScreenCoordFromWorldCoord(player.pos.x, player.pos.y, player.pos.z + 1.20, undefined, undefined);
        if (!result[0])
            return;

        let scale = distanceFromLocal / 25;
        if (scale < 0.5) {
            scale = 0.5;
        }

        if (scale > 0.6)
            scale = 0.6;

        let yModifier = (distanceFromLocal / 25) / 8;
        if (yModifier > 0.05)
            yModifier = 0.05;

        let y = result[2] - yModifier;

        if (y <= 0)
            y = 0;
      
        // Player health starts at 200, subtract 100 to normalize. Divide by 100 (AKA MAX HEALTH) to get the real value.
        let pHealth = ((native.getEntityHealth(player.scriptID) - 100) / 100) * 0.075;

        // Scaling Math
        drawText(player.name, result[1], y, scale, 4, 255, 255, 255, 255, true, false);
        native.drawRect(result[1], y + 0.05, pHealth, 0.01, 255, 0, 0, 100);
    });
});

export function Distance(positionOne, positionTwo) {
	return Math.sqrt(Math.pow(positionOne.x - positionTwo.x, 2) + Math.pow(positionOne.y - positionTwo.y, 2) + Math.pow(positionOne.z - positionTwo.z, 2));
}

export function drawText(msg, x, y, scale, fontType, r, g, b, a, useOutline = true, useDropShadow = true, layer = 0) {
	native.setUiLayer(layer);
	native.beginTextCommandDisplayText('STRING');
	native.addTextComponentSubstringPlayerName(msg);
	native.setTextFont(fontType);
	native.setTextScale(1, scale);
	native.setTextWrap(0.0, 1.0);
	native.setTextCentre(true);
	native.setTextColour(r, g, b, a);

	if (useOutline)
		native.setTextOutline();

	if (useDropShadow)
		native.setTextDropShadow();

	native.endTextCommandDisplayText(x, y);
}