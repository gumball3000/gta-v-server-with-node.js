import * as alt from 'alt';
import * as chat from 'chat';


chat.registerCmd("skin",  (player, args) => {
    if (args[0]){
        if(parseInt(args[0]) >= 1 && parseInt(args[0]) <= 739){
            alt.emitClient(player, "findSkinById", parseInt(args[0]));
        } else {
        alt.emitClient(player, "findSkin", args[0]);
        }
    } else {
        chat.send(player, "/skin «SKIN_MODEL» or /skin «1-739»");  
    }
});

alt.onClient("validSkin", (player, skin)=>{
    player.model = skin;
    chat.send(player, "Skin changed to " + skin);
    alt.emitClient(player, "redoBikeFlag");
})

alt.onClient("invalidSkin", (player, skin)=>{
    chat.send(player, skin + " not found");
})

chat.registerCmd("wep",  (player, arg) => {
    let weaponName;
    if (arg[0]){
        weaponName = arg[0].toLowerCase();
        if (!weaponList[weaponName]){
        chat.send(player, "Weapon not found"); 
        displayWeaponList(player);
        } else {
        player.giveWeapon(weaponList[weaponName], 999, true);
        }
    } else {
        chat.send(player, "/wep «WEAPON_NAME»"); 
    }

});

const weaponList = {
	// melee
	'unarmed': -1569615261,
	'knife':  -1716189206,
	'nightstick': 1737195953,
	'hammer': 1317494643,
	'bat':  -1786099057,
	'crowbar': -2067956739,
	'golfclub': 1141786504,
	'bottle': -102323637,
	'dagger': -1834847097,
	'hatchet': -102973651,
	'knuckleduster': -656458692,
	'machete': -581044007,
	'flashlight': -1951375401,
	'switchblade': -538741184,
	'poolcue': -1810795771,
	'wrench': 419712736,
	'battleaxe': -853065399,
	// pistols
	'pistol': 453432689,
	'pistolmk2': 3219281620,
	'combatpistol': 1593441988,
	'pistol50': -1716589765,
	'snspistol': -1076751822,
	'heavypistol': -771403250,
	'vintagepistol': 137902532,
	'marksmanpistol': -598887786,
	'revolver': -1045183535,
	'appistol': 584646201,
	'stungun': 911657153,
	'flaregun': 1198879012,
	// machine guns
	'microsmg': 324215364,
	'machinepistol': -619010992,
	'smg': 736523883,
	'smgmk2': 2024373456,
	'assaultsmg': -270015777,
	'combatpdw': 171789620,
	'mg': -1660422300,
	'combatmg': 2144741730,
	'combatmgmk2': 3686625920,
	'gusenberg': 1627465347,
	'minismg': -1121678507,
	// assault rifles
	'assaultrifle': -1074790547,
	'assaultriflemk2': 961495388,
	'carbinerifle': -2084633992,
	'carbineriflemk2': 4208062921,
	'advancedrifle': -1357824103,
	'specialcarbine': -1063057011,
	'bullpuprifle': 2132975508,
	'compactrifle': 1649403952,
	// snipers
	'sniperrifle': 100416529,
	'heavysniper': 205991906,
	'heavysnipermk2': 177293209,
	'marksmanrifle': -952879014,
	// shotguns
	'pumpshotgun': 487013001,
	'sawnoffshotgun': 2017895192,
	'bullpupshotgun': -1654528753,
	'assaultshotgun': -494615257,
	'musket': -1466123874,
	'heavyshotgun': 984333226,
	'doublebarrelshotgun': -275439685,
	'autoshotgun': 317205821,
	// heavy weapons
	'grenadelauncher': -1568386805,
	'rpg': -1312131151,
	'minigun': 1119849093,
	'firework': 2138347493,
	'railgun': 1834241177,
	'hominglauncher': 1672152130,
	'grenadelaunchersmoke': 1305664598,
	'compactlauncher': 125959754,
	// thrown
	'grenade': -1813897027,
	'stickybomb': 741814745,
	'proximitymine': -1420407917,
	'bzgas': -1600701090,
	'molotov': 615608432,
	'fireextinguisher': 101631238,
	'petrolcan': 883325847,
	'flare': 1233104067,
	'ball': 600439132,
	'snowball': 126349499,
	'smokegrenade': -37975472,
	'pipebomb': -1169823560,
	// utility
	'parachute': -72657034,
};

function displayWeaponList(player){
    let str = ""
    Object.keys(weaponList).forEach((key)=>{
        str += key
        str += ", "
      })
      str.substring(0, str.length - 1);
      chat.send(player, str); 
}

