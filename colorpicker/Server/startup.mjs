import * as alt from 'alt';
import * as chat from 'chat';

alt.on('playerEnteredVehicle', (player, vehicle, seat) => {
    alt.emitClient(player, "showTheSpeedo");
});

alt.on('playerLeftVehicle', (player, vehicle, seat) => {
    alt.emitClient(player, "hideTheSpeedo");
});

chat.registerCmd("carcolor",  (player) => {
    alt.emitClient(player, "showColorPicker", 1);
});

alt.onClient("selectColor1", (player, x)=>{
    if (player.vehicle === undefined || player.vehicle === null){
        return;
    }

    player.vehicle.customPrimaryColor = x;
    player.vehicle.tireSmokeColor = x;
})

alt.onClient("selectColor2", (player, x)=>{
    if (player.vehicle === undefined || player.vehicle === null){
        return;
    }

    player.vehicle.customSecondaryColor = x;
})
