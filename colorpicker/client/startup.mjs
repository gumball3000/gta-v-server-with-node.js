import * as alt from 'alt';
import * as native from 'natives';

const view = new alt.WebView("http://resources/colorpicker/client/index.html");
let pickingColor = false
alt.onServer("showColorPicker", (carColorIndex)=>{
    view.emit("show"); 
    view.focus(); 
    pickingColor = true
    alt.showCursor(true)
})

view.on("selectColor1", (x)=>{
    alt.emitServer("selectColor1", x);
    view.emit("hide");
    alt.showCursor(false)
    pickingColor = false;
})

view.on("selectColor2", (x)=>{
    alt.emitServer("selectColor2", x);
    view.emit("hide");
    alt.showCursor(false)
    pickingColor = false;
})

// alt.on("update", ()=>{
//     if (pickingColor){
//         native.showCursorThisFrame();    
//     }
// })

